# Fill in your package name and path (identical to PROJECT if there are not subpackages)
PACKAGE = mypackage

.PHONY: install test coverage docs pep8 clean

all: test pep8 docs

install:
	pip install .

test:
	python -m unittest discover -s tests

coverage:
	coverage run -m unittest discover -s tests
	coverage report

pep8:
	autopep8 -r -i $(PACKAGE)
	flake8 $(PACKAGE)

docs:
	make -C docs/ html

clean:
	find $(PROJECT) tests -name '__pycache__' -name '*.pyc' -exec rm '{}' +
	rm -rf build/ dist/


