def hello(verbose=False):
    if verbose:
        return 'Hello world!'

def main():
    import argh
    argh.dispatch_command(hello)
