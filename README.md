# Template python package

[![license](https://img.shields.io/pypi/l/atooms.svg)](https://en.wikipedia.org/wiki/GNU_General_Public_License)
[![pipeline status](https://framagit.org/coslo/template-python/badges/main/pipeline.svg)](https://framagit.org/coslo/template-python/-/commits/main)
[![coverage report](https://framagit.org/coslo/template-python/badges/main/coverage.svg)](https://framagit.org/coslo/template-python/-/commits/main)

This is a template python package. Clone it, remove the `.git` folder and adapt it to your needs. Make sure to replace all instances of  `mypackage` with your package name.

## Quick start

```python
import mypackage
```

## Features

- ...
- ...

## Documentation

Check out the [tutorial]() for more examples and the [public API]() for full details.

## Installation

From pypi
```
pip install mypackage
```

## Contributing

Contributions to the project are welcome. If you wish to contribute, check out [these guidelines]().

## Authors

- ...
- ...