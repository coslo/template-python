import unittest
from mypackage import cli

class Test(unittest.TestCase):

    def setUp(self):
        pass

    def test_hello(self):
        self.assertEqual(cli.hello(verbose=True), 'Hello world!')
        
    def tearDown(self):
        pass
